import os
import pickle
import socket
import sys
import time

from PyQt5 import QtWidgets, uic, QtCore
from PyQt5.QtCore import QUrl, QObject

from util import StoppableThread


class ServerThread(StoppableThread):
    def __init__(self, parent):
        super(ServerThread, self).__init__()
        self._parent = parent
        self._sock = None
        self._conn = None

    def quit(self):
        self.removeFile()
        super(ServerThread, self).quit()
        if (self._sock is not None):
            self._sock.close()
            self._sock = None
        if (self._conn is not None):
            self._conn.close()
            self._conn = None

    def connection(self):
        return self._conn

    def send(self, data):
        try:
            self.connection().send(pickle.dumps(data))
        except Exception as e:
            print('send_exception: ', e)

    def createFile(self, port):
        try:
            f = open('img/port.txt', 'w')
            f.write(str(port))
            f.close()
        except Exception as e:
            print('create_file ', e)

    def removeFile(self):
        try:
            os.remove('img/port.txt')
        except Exception as e:
            print('remove_file ', e)

    def accept_connection(self):
        try:
            self._conn, addr = self._sock.accept()
            return True
        except Exception as e:
            print('accept_connecton ', e)
            return False

    def listen(self):
        while not self.stopped():
            try:
                data = self.connection().recv(1024)
                data = pickle.loads(data)
                print(data)
            except Exception as e:
                print('listen_ex ', e)
                if self._conn is not None:
                    self._conn.close()
                    self._conn = None
                return

    def run(self):
        self.removeFile()
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.bind(('localhost', 0))
        self.createFile(self._sock.getsockname()[1])
        self._sock.listen(1)
        while not self.stopped():
            if self.accept_connection():
                self.listen()


class DrawThread(StoppableThread):
    redraw = QtCore.pyqtSignal(int, int, float, float)

    def __init__(self, parent, window):
        super(DrawThread, self).__init__()
        self._parent = parent
        self._window = window

    def run(self):
        offset = 417 / 120
        while not self.stopped():
            self.redraw.emit(round(86 + self._parent.current_pos * offset),
                             round(80 + self._parent.current_pos * offset),
                             self._parent.current_a,
                             self._parent.current_p)
            self.sleep(0.05)


class PlayerThread(StoppableThread):
    def __init__(self, window):
        super(PlayerThread, self).__init__()
        self._pause = True
        self._server = None
        self._window = window
        self._draw = None
        self.current_pos = 0
        self.current_a = 0
        self.current_p = 0

    def pause(self):
        self._pause = True

    def resume(self):
        self._pause = False

    def quit(self):
        self._server.quit()
        self._draw.quit()
        super(PlayerThread, self).quit()

    def run(self):
        self._server = ServerThread(self)
        self._server.start()
        self._draw = DrawThread(self, self._window)
        self._draw.start()
        while not self.stopped():
            if not self._pause:
                window.lock()
                self.current_pos = 0
                while self.current_pos < 120:
                    while (self._pause):
                        self.sleep(0.1)
                    aIdx = self.getIdx(alphaPoints, self.current_pos)
                    pIdx = self.getIdx(puPoints, self.current_pos)
                    self.current_a = self.linearFx(alphaPoints[aIdx][0], alphaPoints[aIdx + 1][0],
                                      alphaPoints[aIdx][1], alphaPoints[aIdx + 1][1], self.current_pos)
                    self.current_p = self.linearFx(puPoints[pIdx][0], puPoints[pIdx + 1][0],
                                      puPoints[pIdx][1], puPoints[pIdx + 1][1], self.current_pos)
                    self._server.send([self.current_a, self.current_p])
                    self.current_pos += 0.15
                    self.sleep(0.15)
                self._server.send([0,0])
                window.alphaPlay.setGeometry(QtCore.QRect(86, 21, 2, 188))
                window.puPlay.setGeometry(QtCore.QRect(80, 251, 2, 188))
                self._pause = True
                window.unlock()
            self.sleep(0.1)

    def getIdx(self, l, x):
        for i, k in enumerate(l):
            if k[0] > x:
                return i - 1
        return len(l) - 1

    def linearFx(self, x0, x1, fx0, fx1, x):
        return fx0 + (fx1 - fx0) / (x1 - x0) * (x - x0)


class ExampleApp(QtWidgets.QMainWindow):  # Mine Window class
    def __init__(self):
        super().__init__()
        uic.loadUi('graph.ui', self)
        self.setWindowTitle('Графики')
        self.alphaGraph.setSource(QUrl('Graph.qml'))
        self.puGraph.setSource(QUrl('graph1.qml'))
        self.alphaMouseHandler = self.alphaGraph.rootObject().findChild(QObject, 'mouse')
        self.alphaMouseHandler.addPointEvent.connect(addAlphaPoint)
        self.alphaMouseHandler.replacePointEvent.connect(replaceAlphaPoint)
        self.alphaMouseHandler.removePointEvent.connect(removeAlphaPoint)
        self.puMouseHandler = self.puGraph.rootObject().findChild(QObject, 'mouse')
        self.puMouseHandler.addPointEvent.connect(addPuPoint)
        self.puMouseHandler.replacePointEvent.connect(replacePuPoint)
        self.puMouseHandler.removePointEvent.connect(removePuPoint)
        self.exitButton.clicked.connect(quit)
        self.startButton.clicked.connect(start)
        self.stopButton.clicked.connect(stop)
        self.alphaPlay.hide()
        self.puPlay.hide()
        #self.alphaLabel.hide()
        #self.puLabel.hide()
        self.alphaLabel.setText(str(0.0))
        self.puLabel.setText(str(0.0))

    def lock(self):
        self.alphaGraph.setEnabled(False)
        self.puGraph.setEnabled(False)
        self.alphaPlay.show()
        self.puPlay.show()
        self.alphaLabel.show()
        self.puLabel.show()

    def unlock(self):
        self.alphaGraph.setEnabled(True)
        self.puGraph.setEnabled(True)
        self.alphaPlay.hide()
        self.puPlay.hide()
        #self.alphaLabel.hide()
        #self.puLabel.hide()


def addAlphaPoint(idx, x, y):
    alphaPoints.insert(idx, (x, y))


def replaceAlphaPoint(idx, x, y):
    alphaPoints[idx] = (x, y)


def removeAlphaPoint(idx):
    del alphaPoints[idx]


def addPuPoint(idx, x, y):
    puPoints.insert(idx, (x, y))


def replacePuPoint(idx, x, y):
    puPoints[idx] = (x, y)


def removePuPoint(idx):
    del puPoints[idx]


def redraw(pos1, pos2, a, p):
    window.alphaPlay.setGeometry(QtCore.QRect(pos1, 21, 2, 188))
    window.puPlay.setGeometry(QtCore.QRect(pos2, 251, 2, 188))
    window.alphaLabel.setText(str(round(a,3)))
    window.puLabel.setText(str(round(p,3)))


def start():
    player.resume()


def stop():
    player.pause()


def quit():
    player.quit()
    sys.exit()


def main():
    global window, alphaPoints, puPoints, player
    app = QtWidgets.QApplication(sys.argv)  # Новый экземпляр QApplication
    window = ExampleApp()  # Создаём объект класса ExampleApp
    #alphaPoints = [(0, 0), (120, 0)]
    #puPoints = [(0, 0), (120, 0)]
    alphaPoints = [
        (0, 0),
        (0.16*60, 0),
        (0.4*60, 30),
        (1*60, 30),
        (1.2*60, 0),
        (1.3*60, 0),
        (1.43*60, -10),
        (1.67*60, -10),
        (1.88*60, 0),
        (2*60, 0)
    ]
    puPoints = [
        (0 * 60, 0),
        (0.17 * 60, 0),
        (0.5 * 60, 5),
        (0.9 * 60, 5),
        (1.2 * 60, 0),
        (1.4 * 60, 0),
        (1.5 * 60, -2),
        (1.75 * 60, -2),
        (1.9 * 60, 0),
        (2 * 60, 0)
    ]
    player = PlayerThread(window)
    player.start()
    time.sleep(0.1)
    player._draw.redraw.connect(redraw)
    window.show()  # Показываем окно
    app.exec_()  # и запускаем приложение
    player.quit()


if __name__ == '__main__':
    main()
