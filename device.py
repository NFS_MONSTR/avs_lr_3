import pickle
import socket
import sys
import time

from PyQt5 import QtWidgets, uic, QtCore
from PyQt5.QtCore import QUrl, QObject
from PyQt5.QtQuick import QQuickView

from util import StoppableThread

class ClientThread(StoppableThread):
    def __init__(self, parent):
        super(ClientThread, self).__init__()
        self._client = None
        self._parent = parent
        self.connected = False

    def quit(self):
        if self._client is not None:
            self._client.close()
            self._client = None
        super(ClientThread, self).quit()

    def send(self, data):
        try:
            self._client.send(pickle.dumps(data))
        except Exception as e:
            print('send ', e)
            self.connected = False
            try:
                self._client.close()
            except:
                pass
            self._client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.lost_connection_handler()

    def listen(self):
        while not self.stopped() and self.connected:
            data = self._client.recv(1024)
            data = pickle.loads(data)
            self._parent.alpha = data[0]
            self._parent.pu = data[1]
            print(data)

    def get_port(self):
        while not self.stopped():
            try:
                f = open('img/port.txt')
                port = int(f.readline())
                f.close()
                return port
            except Exception as e:
                print('get_port ', e)
            self.sleep(0.3)
        return 0

    def run(self):
        self._client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # self.connect()
        # self.listen()
        self.connected = False
        while not self.stopped():
            if not self.connected:
                try:
                    port = self.get_port()
                    if port == 0:
                        continue
                    self._client.connect(('localhost', port))
                    self.connected = True
                except Exception as e:
                    print('connect ', e)
            else:
                try:
                    self.listen()
                except Exception as e:
                    print('listen ', e)
                    self._client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    self.connected = False
                    self.lost_connection_handler()
                self.sleep(0.3)

    def lost_connection_handler(self):
        self._parent.alpha = 0
        self._parent.pu = 0

class DrawThread(StoppableThread):
    redraw = QtCore.pyqtSignal(float, float)

    def __init__(self, parent, window):
        super(DrawThread, self).__init__()
        self._parent = parent
        self._window = window

    def run(self):
        while not self.stopped():
            self.redraw.emit(self._parent.alpha, self._parent.pu)
            self.sleep(0.05)

class DeviceThread(StoppableThread):#По факту не нужен но мне похуй :\
    def __init__(self, window):
        super(DeviceThread, self).__init__()
        self._pause = True
        self._client = None
        self._window = window
        self.alpha = 0
        self.pu = 0

    def pause(self):
        self._pause = True

    def resume(self):
        self._pause = False

    def quit(self):
        self._client.quit()
        self._draw.quit()
        super(DeviceThread, self).quit()

    def run(self):
        self._client = ClientThread(self)
        self._client.start()
        self._draw = DrawThread(self, self._window)
        self._draw.start()
        while not self.stopped():
            #if not self._pause:
            self.sleep(0.1)

class DeviceWindow(QQuickView):

    def getObj(self, name):
        return self.rootObject().findChild(QObject, name)

    def setObjVisibility(self, obj, state):
        obj.setProperty('visible', state)

    def setValue(self, idx, value):
        self.arrows[idx].setProperty('value', value)
        self.setObjVisibility(self.labels[idx], value > self.maxValue[idx] or value < self.minValue[idx])

    def init(self):
        self.alphaArrow = self.getObj('alphaArrow')
        self.puArrow = self.getObj('puArrow')
        self.alphaLabel = self.getObj('alphaLabel')
        self.puLabel = self.getObj('puLabel')
        self.arrows = [self.alphaArrow, self.puArrow]
        self.labels = [self.alphaLabel, self.puLabel]
        self.maxValue = [25, 4]
        self.minValue = [-5, -1]
        self.setTitle("Прибор")


def redraw(val1, val2):
    window.setValue(0, val1)
    window.setValue(1, val2)


def main():
    global window
    app = QtWidgets.QApplication(sys.argv)
    window = DeviceWindow()
    window.setSource(QUrl('Device.qml'))
    window.init()
    device = DeviceThread(window)
    device.start()
    time.sleep(0.1)
    device._draw.redraw.connect(redraw)
    window.show()
    app.exec_()
    device.quit()


if __name__ == '__main__':
    main()
