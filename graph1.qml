import QtQuick 2.4
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4
import QtCharts 2.14

Item {
    ChartView {
        objectName: "chart"
        antialiasing: true
        legend.visible: false
        width: 481
        height: 221
        margins.top: 0
        margins.bottom: 0
        margins.left: 0
        margins.right: 0
        LineSeries {
            id: lineSeries
            XYPoint { x: 0; y: 0 }
            XYPoint { x: 0.17; y: 0 }
            XYPoint { x: 0.5; y: 5 }
            XYPoint { x: 0.9; y: 5 }
            XYPoint { x: 1.2; y: 0 }
            XYPoint { x: 1.4; y: 0 }
            XYPoint { x: 1.5; y: -2 }
            XYPoint { x: 1.75; y: -2 }
            XYPoint { x: 1.9; y: 0}
            XYPoint { x: 2; y: 0 }
            axisY: axisY
        }
        ValueAxis {
            id: axisY
            min: -2
            max: 5
            tickInterval: 1
            tickCount: 8
        }
        MouseArea {
            objectName: "mouse"
            signal addPointEvent(int idx, real x, real y)
            signal replacePointEvent(int idx, real x, real y)
            signal removePointEvent(int idx)
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton | Qt.RightButton
            onClicked: {
                return;//disable editing
                let x = mouseX-parent.plotArea.left
                let y = (parent.plotArea.top+parent.plotArea.bottom)/2-mouseY
                let width = parent.plotArea.right - parent.plotArea.left+1
                let height = parent.plotArea.bottom - parent.plotArea.top+1
                let xVal = 2/width*x
                let yVal = 7/height*y+1.5
                if (mouse.button & Qt.LeftButton) {
                    let n = lineSeries.count-1
                    let replace = false
                    let replaceDelta = 0.03
                    if (xVal>2+replaceDelta || xVal<0-replaceDelta)
                        return
                    for (let i = 0; i<lineSeries.count; i++) {
                        let point = lineSeries.at(i)
                        if (Math.abs(point.x-xVal)<replaceDelta) {
                            replace = true
                            n = i
                            break
                        }
                        if (point.x>xVal) {
                            n = i
                            break
                        }
                    }
                    if (replace) {
                        let p = lineSeries.at(n)
                        lineSeries.replace(p.x,p.y,p.x, yVal)
                        replacePointEvent(n, p.x*60, yVal)
                    } else {
                        lineSeries.insert(n,xVal,yVal)
                        addPointEvent(n, xVal*60, yVal)
                    }
                } else {
                    let idx = -1
                    for (let i = 0; i<lineSeries.count; i++) {
                        let point = lineSeries.at(i)
                        let px = point.x/2*width
                        let py = (point.y-1.5)/7*height
                        if (Math.hypot(px-x,py-y)<10) {
                            idx = i
                            break
                        }
                    }
                    if (idx>-1 && idx!=0 && idx!=lineSeries.count-1) {
                        lineSeries.remove(idx)
                        removePointEvent(idx)
                    }
                }
             }
        }
    }
}
