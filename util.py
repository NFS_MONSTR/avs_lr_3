from threading import Thread, Event

from PyQt5 import QtCore


class StoppableThread(Thread, QtCore.QObject):
    def __init__(self):
        Thread.__init__(self)
        QtCore.QObject.__init__(self)
        self._stop = Event()
        self._stop.clear()

    def quit(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()

    def sleep(self, time):
        self._stop.wait(time)
