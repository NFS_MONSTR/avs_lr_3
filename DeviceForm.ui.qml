import QtQuick 2.4
import QtQuick.Extras 1.4
import QtQuick.Controls 2.4
import QtQuick.Controls.Styles 1.4

Rectangle {
    width: 350
    height: 350
    color: "#000"

    Label {
        x: 22
        y: 22
        text: "α"
        color: "#ffF"
        font.pixelSize: 22
    }

    Label {
        x: 300
        y: 22
        text: "Пу"
        color: "#ffF"
        font.pixelSize: 22
    }

    Image {
        id: alphaLabel
        objectName: "alphaLabel"
        x: 115
        y: 230
        fillMode: Image.PreserveAspectFit
        source: "img/alpha.png"
        visible: false
    }

    Image {
        id: puLabel
        objectName: "puLabel"
        x: 190
        y: 230
        fillMode: Image.PreserveAspectFit
        source: "img/pu.png"
        visible: false
    }

    CircularGauge {
        objectName: "alphaArrow"
        x: 22
        y: 22
        width: 300
        height: 300
        value: 1
        minimumValue: -5
        maximumValue: 25
        style: CircularGaugeStyle {
                minimumValueAngle: 183
                maximumValueAngle: 345
                labelStepSize: 5
                tickmarkStepSize: 5
                foreground: Item {
                    Rectangle {
                        visible: false
                    }
                }
                needle: Item {
                        implicitWidth: 10
                        implicitHeight: 112
                        y: 5
                        Image {
                            fillMode: Image.PreserveAspectFit
                            source: "img/arrowSmall.png"
                        }
                }
        }
    }

    CircularGauge {
        objectName: "puArrow"
        x: 22
        y: 22
        width: 300
        height: 300
        value: 0
        minimumValue: -1
        maximumValue: 4
        style: CircularGaugeStyle {
                minimumValueAngle: 155
                maximumValueAngle: 13
                labelStepSize: 1
                tickmarkStepSize: 1
                foreground: Item {
                    Rectangle {
                        visible: false
                    }
                }
                needle: Item {
                        implicitWidth: 10
                        implicitHeight: 112
                        y: 5
                        Image {
                            fillMode: Image.PreserveAspectFit
                            source: "img/arrowSmall.png"
                        }
                }
        }
    }
}
